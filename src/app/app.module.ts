import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavLoginComponent } from './commons/navbars/nav-login/nav-login.component';
import { SigninComponent } from './pages/signin/signin.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { JumbotronComponent } from './commons/jumbotron/jumbotron.component';
import { FindPnrComponent } from './commons/find-pnr/find-pnr.component';
import { AtentionHistoryComponent } from './commons/atention-history/atention-history.component';
import { AtentionStadisticComponent } from './commons/atention-stadistic/atention-stadistic.component';
import { ItineraryInfoComponent } from './commons/itinerary-info/itinerary-info.component';
import { DetailPnrComponent } from './pages/detail-pnr/detail-pnr.component';


@NgModule({
  declarations: [
    AppComponent,
    NavLoginComponent,
    SigninComponent,
    DashboardComponent,
    JumbotronComponent,
    FindPnrComponent,
    AtentionHistoryComponent,
    AtentionStadisticComponent,
    DetailPnrComponent,
    ItineraryInfoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    NgxCaptchaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
