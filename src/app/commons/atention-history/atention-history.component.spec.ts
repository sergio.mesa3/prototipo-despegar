import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtentionHistoryComponent } from './atention-history.component';

describe('AtentionHistoryComponent', () => {
  let component: AtentionHistoryComponent;
  let fixture: ComponentFixture<AtentionHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtentionHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtentionHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
