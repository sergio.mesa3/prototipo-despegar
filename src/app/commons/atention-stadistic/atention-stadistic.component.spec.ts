import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtentionStadisticComponent } from './atention-stadistic.component';

describe('AtentionStadisticComponent', () => {
  let component: AtentionStadisticComponent;
  let fixture: ComponentFixture<AtentionStadisticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtentionStadisticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtentionStadisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
