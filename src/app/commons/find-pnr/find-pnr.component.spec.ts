import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindPnrComponent } from './find-pnr.component';

describe('FindPnrComponent', () => {
  let component: FindPnrComponent;
  let fixture: ComponentFixture<FindPnrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindPnrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindPnrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
