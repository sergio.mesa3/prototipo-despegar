import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPnrComponent } from './detail-pnr.component';

describe('DetailPnrComponent', () => {
  let component: DetailPnrComponent;
  let fixture: ComponentFixture<DetailPnrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPnrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPnrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
