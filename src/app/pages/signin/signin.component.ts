import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  showPassType = 'password';

  constructor(
    private route: Router
  ) { }

  ngOnInit() {
  }

  goToDashboard(){
    this.route.navigate([
      "dashboard"
    ]);
  }

  showPass() {
    if (this.showPassType === 'password') {
      this.showPassType = 'text';
    } else {
      this.showPassType = 'password';
    }
  }

}
